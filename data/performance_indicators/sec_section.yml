- name: Sec - Section CMAU - Reported Sum of Sec Section SMAU
  base_path: "/handbook/product/performance-indicators/"
  definition: A sum of all SMAUs across all stages in the Sec Section (Secure, Protect) for All users and Paid users.
  target: 
  org: Sec Section
  section: Sec
  public: true
  pi_type: Section CMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - currently instrumented to meet OKRs (see instrumentation, Lessons Learned for more details)
  implementation:
    status: Complete
    reasons:
    - Can't currently differentiate between free-to-OSS/education Gold/Ultimate users and paid Gold/Ultimate users
    - Threat Insights data is not yet included in this count until we support user-level metrics in Snowplow for self-managed
    lessons:
      learned:
      - Current Self-Managed Uplift is leading to massive underreporting as it doesn't fully account for non-reporting instances (offline environments, security conscious enterprises)
  monthly_focus:
    goals:
      - Identify a way to denote underreporting on Secure-related *MAU charts to remove confusion / clarify metrics interpretation
      - Identify [usage by product tier](https://gitlab.com/gitlab-data/analytics/-/issues/6972) while removing free-to-OSS/education Gold/Ultimate users
      - Support Data team on improving Self-Managed Uplift for Secure
  metric_name: user_unique_users_all_secure_scanners
  sisense_data:
    chart: 9982146
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10224291
    dashboard: 758607
    embed: v2
- name: Sec, Secure - Section MAU, SMAU - Unique users who have used a Secure scanner 
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The number of unique users who have run one or more Secure scanners.
  target: 10% higher than current month (SaaS and self-managed combined)
  org: Sec Section
  section: sec
  stage: secure
  public: true
  pi_type: Section MAU, SMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - currently instrumented to meet OKRs (see instrumentation, Lessons Learned for more details)
  implementation:
    status: Complete
    reasons:
    - Can't currently differentiate between free-to-OSS/education Gold/Ultimate users and paid Gold/Ultimate users
    - Threat Insights data is not yet included in this count until we support user-level metrics in Snowplow for self-managed
    lessons:
      learned:
      - Current Self-Managed Uplift is leading to massive underreporting as it doesn't fully account for non-reporting instances (offline environments, security conscious enterprises)
  monthly_focus:
    goals:
      - Identify a way to denote underreporting on Secure-related *MAU charts to remove confusion / clarify metrics interpretation
      - Identify [usage by product tier](https://gitlab.com/gitlab-data/analytics/-/issues/6972) while removing free-to-OSS/education Gold/Ultimate users
      - Support Data team on improving Self-Managed Uplift for Secure
  metric_name: user_unique_users_all_secure_scanners
  sisense_data:
    chart: 9982579
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 9984584
    dashboard: 758607
    embed: v2

- name: Secure:Static Analysis - GMAU - Users running Static Analysis jobs
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The highest of the number of unique users who have run SAST or Secret Detection jobs.
  target: Progressively increasing month-over-month, >10%
  org: Sec Section
  section: sec
  stage: secure
  group: static_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track, continued healthy growth of static analysis usage. 
  implementation:
    status: Complete
    reasons:
    - Can't currently differentiate between free OSS Ultimate users and paid Ultimate users.
  lessons:
    learned:
      - Tracking upgrades is really difficult to today, but we're [starting to get directional data](https://app.periscopedata.com/app/gitlab/718481/Static-Analysis-Metrics---@tmccaslin?widget=11692591&udv=1090066) to better understand how many and what vectors upgrades from SAST come from.
  monthly_focus:
    goals:
      - Explore upgrade paths for non-ultimate customers discovering the value of ultimate and upgrading. We're [working with the verify team](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/360) to explore ways to encourage SAST usage and hopefully better track upgrades.
      - Moving forward we are focused strongly on data accuracy and reduction of false positives. We have a [new exploration to validate a new vulnerability tracking improvement from Vulnerability Research](https://gitlab.com/groups/gitlab-org/-/epics/5144) which could materially reduce false positives. This approach is appearing to work out and produce more accurate tracking. This also opens a new metrics problem that we need a way to track accuracy with real-world data. We intend to measure the improvements of this change via the [new vulnerabilities data](https://app.periscopedata.com/app/gitlab/801442/Taylors-Vuln-Exploration) in Sisence to ensure this change results in material positive impact before rolling it out. This feature has gotten heavily delayed due to rapid action DB work and upstream bugs with vulnerability reports that our group does not directly control. 
      - To increase efficiency and reduce maintenance costs of our 10 linter analyzers we are [transitioning to SemGrep for many of our linter analyzers](https://gitlab.com/groups/gitlab-org/-/epics/5245). If successful we will reduce ~10 open source analyzers into 1 while also improving the detection rules and proving a more consistent rule management experience across these analyzers, this will be materially impactful as we seek to build our proprietary SAST engine on top of these results. This effort is on track and we will be GAing the Semgrep analyzer in 13.12. [Experimental](https://docs.gitlab.com/ee/user/application_security/sast/#experimental-features) availibility is [generating a good amount of usage(https://app.periscopedata.com/app/gitlab/718481/Static-Analysis-Metrics---@tmccaslin?widget=11703484&udv=1090066).  
  metric_name: user_sast_jobs, user_secret_detection_jobs
  sisense_data:
    chart: 9980130
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 9967764
    dashboard: 758607
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670

- name: Secure:Static Analysis - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-static_analysis/stage-groups-group-dashboard-secure-static-analysis)

- name: Secure:Dynamic Analysis - GMAU - Users running DAST
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: Number of unique users who have run one or more DAST jobs.
  target: Progressively increasing month-over-month, >10% per-month
  org: Sec Section
  section: sec
  stage: secure
  group: dynamic_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track, usage continues to trend upwards.
  implementation:
    status: Complete
    reasons:
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
  lessons:
    learned:
      - Reported self-managed usage jumped significantly in April. The reason for this is unknown and could be due to usage ping being enabled on more instances, new customers, or more awareness of DAST capabilities. Investigation into the sudden increase is on-going.
      - Customers have stopped using DAST in the past because of the huge number of vulnerabilities produced in the dashboard, vulnerability aggregation is the first step in addressing this.
  monthly_focus:
    goals:
      - Aggregating noisy vulnerabilities to stop DAST from overwhelming the Vulnerability Report (https://gitlab.com/gitlab-org/gitlab/-/issues/254043)
      - Add more options to Site profile (https://gitlab.com/groups/gitlab-org/-/epics/3771)
  metric_name: user_dast_jobs
  sisense_data:
    chart: 9980137
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 9980139
    dashboard: 758607
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670

- name: Secure:Dynamic Analysis - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-dynamic_analysis/stage-groups-group-dashboard-secure-dynamic-analysis)

- name: Secure:Composition Analysis - GMAU - Users running any SCA scanners
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more Dependency Scanning jobs, or number of unique users who have run one or more License Scanning jobs.
  target: 2% higher than current month (SaaS and self-managed combined) for All and Paid
  org: Sec Section
  section: sec
  stage: secure
  group: composition_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  implementation:
    status: Complete
    reasons:
    - 13.11 and 13.12 team was dedicated to rapid action, no PI work in place. Currently our data is MVC and a lagging indicator, trends are accurate but due to extrapolation of self-hosted, low opt-in of self-hosted, overall missing granularity, and 28 day cycles actual numbers are imprecise, in addition I believe there was another data event this month resulting in not being able to view SaaS, but using my own data charts as well as this charts self hosted data, it allows high level trending comparisons.
  lessons:
    learned:
      - Nothing through use of xMAU Metrics.
  monthly_focus:
    goals:
      - Handle the rapid action event and then focus on [Software Composition Analysis removals and deprecations for 14.0](https://about.gitlab.com/blog/2021/02/08/composition-analysis-14-deprecations-and-removals/). We hope there is then time for [Consistency in default behaviour of AST scanners and jobs](https://gitlab.com/groups/gitlab-org/-/epics/5334). After which we will resume to our usual, nothing directly related to these lagging indicators, but continue work on [Dependency Scanning to complete](https://gitlab.com/groups/gitlab-org/-/epics/1664) however [auto remediation](https://gitlab.com/groups/gitlab-org/-/epics/3188) is still blocked by a token issue.
  metric_name: user_license_management_jobs, user_dependency_scanning_jobs
  sisense_data:
    chart: 9967897
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10102606
    dashboard: 758607
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670

- name: Secure:Composition Analysis - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-composition_analysis/stage-groups-group-dashboard-secure-composition-analysis)

- name: Secure:Fuzz Testing - GMAU - Users running fuzz testing
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more Fuzz
    Testing jobs.
  target: Progressively increasing month-over-month, >10% per-month
  org: Sec Section
  section: sec
  stage: secure
  group: fuzz_testing
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - We have a good growth rate and need more users to use fuzz testing.
  implementation: 
    status: Definition
    reasons:
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
  lessons:
    learned:
      - Data [integrity issues](https://gitlab.com/gitlab-data/analytics/-/issues/8218) with processing usage ping means we aren't confident in this month's data or its relationship to previous trends.
  monthly_focus:
    goals:
      - Corpus management for coverage-guided fuzz testing (https://gitlab.com/groups/gitlab-org/-/epics/2915)
      - Add support for AFL fuzz targets (https://gitlab.com/gitlab-org/gitlab/-/issues/321075)
      - Improve our documentation based on known pain points (https://gitlab.com/gitlab-org/ux-research/-/issues/1273)
      - Continue analysis of new data to understand how often and how many users successfully use fuzz testing.
      - Monitor inflow of new data and update performance indicator charts as needed.
      - Any changes needed to restore or update reporting as a result of the outages.
  metric_name: user_coverage_fuzzing_jobs, user_api_fuzzing_jobs, user_api_fuzzing_dnd_jobs on self-managed, (options like '%gitlab-cov-fuzz%' OR SECURE_CI_JOB_TYPE='api_fuzzing') on .com
  sisense_data:
    chart: 9842394
    dashboard: 707777
    embed: v2
  sisense_data_secondary:
    chart: 9926832
    dashboard: 707777
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670

- name: Secure:Fuzz Testing - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 3
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-fuzz_testing/stage-groups-group-dashboard-secure-fuzz-testing)

- name: Secure:Threat Insights - GMAU - Users interacting with Secure UI
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: Number of unique sessions viewing a security dashboard, pipeline security
    report, or expanding MR security report. Same as Paid until OSS projects
    can be separated.
  target: Progressively increasing month-over-month, >2% per-month
  org: Sec Section
  section: sec
  stage: secure
  group: threat_insights
  public: true
  pi_type: GMAU
  product_analytics_type: SaaS
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - GMAU currently showing ~6% dip, reversing 6-month trend of gains. May be related to current days-long lag in metrics data refresh.
  implementation:
    status: Definition
    reasons:
    - Collection is complete for .com only
    - Snowplow data is session only, not unique users
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
    - Cannot separate out GitLab employee usage from customers
  lessons:
    learned:
      - Even if data for April is incomplete, overall page views are up at least 7%, pointing to continued rise in  individual user engagement
      - Increase was smaller but Project Vulnerability Reports and MR security widget usage increased from March
      - Pipeline security views are down while MR views are up, could indicate users who discover the Pipeline view are not getting distinct value
  monthly_focus:
    goals:
      - Continue executing on roadmap based on input from other sensing mechanisms
      - Focus on key technical debt around performance, scalability, and maintainability
      - Evaluate main drivers of [Error Budget overages](https://about.gitlab.com/handbook/product/sec-section-performance-indicators/#securethreat-insights---error-budget-for-gitlabcom) and work to address
  metric_name: n/a
  sisense_data:
    chart: 9244137
    dashboard: 707777
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670

- name: Secure:Threat Insights - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-threat_insights/stage-groups-group-dashboard-secure-threat-insights)

- name: Protect:Container Security - SMAU, GMAU - Users running Container Scanning or interacting with Protect UI
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The number of users who have run a container scanning job or interacted with the Protect UI in the last 28 days of the month.
  target: Progressively increasing month-over-month, >2%
  org: Sec Section
  section: sec
  stage: Protect
  group: container_security
  public: true
  pi_type: SMAU, GMAU
  product_analytics_type: Both
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Usage growth is rapidly accelerating. It is estimated that the uplift is largely due to closed Gold/Ultimate deals in 2020 that are now adopting Secure.
    - The shift from Clair to Trivy has generated a lot of customer interest.  Although we are unable to slice the data by the type of scanning engine used, some of this uplift may be due to customers trying out the new Trivy scanning engine.
  implementation:
    status: Complete
    reasons:
    - Limitations in data (see SCA limitations above)
    - Although we have released the Alert Dashboard, we do not yet have a good way of tracking monthly active users for this area. Instrumentation is expected to be complex (1-2 months of one developer's time) and usage is expected to remain low until additional alert times are added to the dashboard. Currently instrumenting these metrics is a low priority.
  lessons:
    learned:
      - The switch from Clair to Trivy has allowed us to close 20+ outstanding customer issues and bugs.  Adoption is expected to increase significantly after the 14.0 release.
  monthly_focus:
    goals:
      - Project-level DAST Scan Schedule Policies (pipeline) (https://gitlab.com/groups/gitlab-org/-/epics/5329) is available behind a feature flag
      - Project-level DAST Scan Schedule Policies (scheduled scans) (https://gitlab.com/groups/gitlab-org/-/epics/5360) is being worked on for a May release
      - Replacing Clair with Trivy (https://gitlab.com/groups/gitlab-org/-/epics/5398) will be the default beginning in the 14.0 release and will set the foundation for us to be able to scan containers in production environments
  metric_name: user_container_scanning_jobs
  sisense_data:
    chart: 10039269
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039569
    dashboard: 758607
    embed: v2

- name: Protect:Container Security - SMAC, GMAC - Clusters using Container Network or Host Security
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The number of active clusters with at least one Container Security feature
    enabled (Network Policies, or Host Security) in the last 28 days.
  target: Progressively increasing >10% month-over-month
  org: Sec Section
  section: sec
  stage: Protect
  group: container_security
  public: true
  pi_type: SMAC, GMAC
  product_analytics_type: Both
  plan_type: Both
  is_primary: false
  is_key: false
  health:
    level: 3
    reasons:
    - Growth from February -> March saw 4 new clusters start using Container Network Security
    - Growth from March -> April is fairly stagnant.  We lost one cluster, which I suspect is due to an intermittent usage ping from this particular customer (it seems to come and go every other month)
    - The installation of Cilium has been failing into modern versions of Kubernetes, likely explaining the stagnation in new users.  We do not currently have a SET for this area and did not have tests to cover this.
    - We are not actively investing in this area right now, so new feature development has stopped
  implementation:
    status: Complete
    reasons:
    - Data collection for CHS is [planned but not yet implemented](https://gitlab.com/gitlab-org/gitlab/-/issues/218800) as the category is relatively new
  lessons:
    learned:
      - We have 26 clusters using CNS, 23 of which are on gitlab.com and 3 is self managed.  Other self-managed customers may be using CNS but not reporting usage ping.
      - Customers are using the blocking mode as a significant amount of traffic is being dropped.
      - With the Defend -> Protect changes, we have significantly reduced our investment in this area
  monthly_focus:
    goals:
      - We are temporarily not investing in this area so we can allocate more resources to Security Orchestration and Container Security
  metric_name: clusters_applications_cilium
  sisense_data:
    chart: 9620410
    dashboard: 694854
    embed: v2

- name: Protect:Container Security  - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-container_security/stage-groups-group-dashboard-protect-container-security)
